package com.Gorest;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class Delete_gorest {
	@Test
	private void Delete_methods() {

		baseURI = "https://gorest.co.in";
		given().header("Authorization", "Bearer af52f55e082382eb807b4ac58cf064339cf822d07eea1f1974f83da11e407c82")
				.when().delete("/public/v2/users/191478").then().statusCode(204);
	}

}
