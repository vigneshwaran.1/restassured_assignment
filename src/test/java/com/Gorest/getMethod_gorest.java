package com.Gorest;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class getMethod_gorest {
	
	@Test(priority = 1)
	public void GetMethod() {

		RequestSpecification http = RestAssured.given().header("Authorization",
				"e842b0fe20b3f4193a6e90e8fe9561f09e5172813b3fc8ab970b2aa51ca618b6");
		Response response = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = response.getStatusCode();
		System.out.println(statusCode);
		System.out.println(response.getBody().asString());
		Assert.assertEquals(statusCode, 200);
	}

	
	

}
